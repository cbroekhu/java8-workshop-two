package com.workshop.springboot.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ArticleCrudRepository extends CrudRepository<Article, Long> {
	
}
