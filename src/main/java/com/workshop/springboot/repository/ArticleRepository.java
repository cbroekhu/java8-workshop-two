package com.workshop.springboot.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
	
	@Query("SELECT a from Article a where a.id = (:id)")
	Optional<Article> findByIdCustom( @Param("id") long id);
	
}
