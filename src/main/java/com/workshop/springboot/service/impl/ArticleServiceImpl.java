package com.workshop.springboot.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.workshop.springboot.repository.Article;
import com.workshop.springboot.repository.ArticleRepository;
import com.workshop.springboot.service.ArticleService;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService{

	@Autowired
	ArticleRepository articleRepository;
	
	@Override
	public Article findArticle(long id) {
		return articleRepository.findById(id).orElse(null);
	}

	@Override
	public void newArticle(Article article) {
		articleRepository.save(article);
	}

	@Override
	public Article getArticleById(long id) {
		return articleRepository.findById(id).orElse(null);
	}

	@Override
	public void deleteArticle(long id) {
		articleRepository.deleteById(id);
		
	}

	@Override
	public List<Article> getAllArticles() {
		return articleRepository.findAll();
	}

	@Override
	public Article findByIdCustom( long id) {
		return articleRepository.findByIdCustom(id).orElse(null);
	}

}
