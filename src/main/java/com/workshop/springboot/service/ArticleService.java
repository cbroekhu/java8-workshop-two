package com.workshop.springboot.service;

import java.util.List;

import com.workshop.springboot.repository.Article;

public interface ArticleService {

	Article findArticle(long id);

	void newArticle(Article article);

	Article getArticleById(long id);

	void deleteArticle(long id);

	List<Article> getAllArticles();

	Article findByIdCustom(long id);

}
