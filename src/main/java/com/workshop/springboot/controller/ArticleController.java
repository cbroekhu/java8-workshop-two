package com.workshop.springboot.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.workshop.springboot.repository.Article;
import com.workshop.springboot.service.ArticleService;

@RestController
@RequestMapping(path = "/api")
public class ArticleController {

	protected static final Logger logger = LogManager
			.getLogger(ArticleController.class);

	@Autowired
	ArticleService articleService;
	
	// https://localhost:8080/api/article/get1?id=123
	@GetMapping(value = "/article/get1", produces = { "application/json",
			"application/xml" })
	public ResponseEntity<Article> getArticle1(
			@RequestParam(value = "id", required = true) final long id) {

		logger.info("Inside 'getArticle1'");

		Article article = articleService.findArticle(id);

		return ResponseEntity.ok(article);

	}

	// https://localhost:8080/api/article/get/123/
	@GetMapping(value = "/article/get2/{id}", produces = "application/json")
	public ResponseEntity<Article> getArticle2(
			@PathVariable(value = "id") final long id) {

		logger.info("Inside 'getArticle2'");

		// Deze keer een custom repository methode op basis van een query
		Article article = articleService.findByIdCustom(id);

		return ResponseEntity.ok(article);

	}

	@GetMapping(value = "/article/getall", produces = "application/json")
	public ResponseEntity<List<Article>> getAllArticles() {

		logger.info("Inside 'getAllArticles'");

		List<Article> articles = articleService.getAllArticles();

		return ResponseEntity.ok(articles);

	}
	
	@PostMapping(value = "/article/new", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Article> newArticle(@RequestBody final Article article) {

		logger.info("Inside 'newArticle'");

		articleService.newArticle(article);

		return ResponseEntity.ok(article);

	}

	// https://localhost:8080/api/article/delete?id=123
	@GetMapping(value = "/article/delete")
	public ResponseEntity<String> deleteArticle1(
			@RequestParam(value = "id", required = true) final long id) {

		logger.info("Inside 'deleteArticle1'");

		articleService.deleteArticle(id);

		return ResponseEntity.ok("Article with id: " + id + " deleted");

	}

	// https://localhost:8080/api/article/delete/123/
	@GetMapping(value = "/article/delete/{id}")
	public ResponseEntity<String> deleteArticle2(
			@PathVariable(value = "id") final long id) {

		logger.info("Inside 'deleteArticle2'");

		articleService.deleteArticle(id);

		return ResponseEntity.ok("Article with id: " + id + " deleted");

	}

}
